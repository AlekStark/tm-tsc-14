package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.repository.IProjectRepository;
import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectTaskService;
import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.findAllTasksByProjectId(projectId);
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProjectById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskFromProjectById(projectId, taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByIndex(Integer projectIndex) {
        if (projectIndex == null || projectIndex < 0) return null;
        Project project = projectRepository.findProjectByIndex(projectIndex);
        String projectId = project.getId();
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByName(String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        Project project = projectRepository.findProjectByName(projectName);
        String projectId = project.getId();
        taskRepository.removeTasksByProjectId(projectId);
        return projectRepository.removeProjectById(projectId);
    }

}
