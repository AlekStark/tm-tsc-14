package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.api.repository.IProjectRepository;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(Comparator<Project> projectComparator) {
        final List<Project> projects = new ArrayList<>(this.projects);
        projects.sort(projectComparator);
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findProjectById(final String id) {
        for (Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findProjectByName(final String name) {
        for (Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findProjectByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeProjectById(final String id) {
        final Project project = findProjectById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeProjectByName(final String name) {
        final Project project = findProjectByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        final Project project = findProjectByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findProjectById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findProjectByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) {
        final Project project = findProjectById(id);
        if (id == null || id.isEmpty()) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(String name) {
        final Project project = findProjectByName(name);
        if (name == null || name.isEmpty()) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) {
        final Project project = findProjectByIndex(index);
        if (index == null || index < 0) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(String id) {
        final Project project = findProjectById(id);
        if (id == null || id.isEmpty()) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByName(String name) {
        final Project project = findProjectByName(name);
        if (name == null || name.isEmpty()) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByIndex(Integer index) {
        final Project project = findProjectByIndex(index);
        if (index == null || index < 0) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusProjectByIndex(Integer index, Status status) {
        final Project project = findProjectByIndex(index);
        if (index == null || index < 0) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusProjectByName(String name, Status status) {
        final Project project = findProjectByName(name);
        if (name == null || name.isEmpty()) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusProjectById(String id, Status status) {
        final Project project = findProjectById(id);
        if (id == null || id.isEmpty()) return null;
        project.setStatus(status);
        return project;
    }

    public int getCount() {
        return projects.size();
    }

    @Override
    public boolean existsById(String projectId) {
        return findProjectById(projectId) != null;
    }

}
