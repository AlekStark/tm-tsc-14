package com.tsc.afedorovkaritsky.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),

    IN_PROGRESS("In process"),

    COMPLETED("Completed");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
