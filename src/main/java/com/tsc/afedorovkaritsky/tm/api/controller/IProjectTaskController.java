package com.tsc.afedorovkaritsky.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskFromProjectById();

    void findAllTasksByProjectId();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}
